subroutine rk4 (x0, y0, n, xf, func, params, yf)
  use, intrinsic :: iso_fortran_env, wp => real64
  integer,  intent(in)  :: n
  real(wp), intent(in)  :: x0, y0, xf, params(:)
  real(wp), intent(out) :: yf
  real(wp) :: h, k(4)
  real(wp), allocatable :: x(:), y(:)
  integer :: i
  
  interface
    function func (x, y, params)
      import :: wp
      implicit none
      real(wp) :: func, x, y, params(:)
    end function func
  end interface
  
  if (allocated(x)) deallocate(x)
  if (allocated(y)) deallocate(y)
  allocate(x(n), y(n))
  
  h = (xf - x0) / (n - 1)
  x = [ (x0 + i * h, i=1, n) ]
  
  y(1) = y0
  do i=1, n-1
     k(1) = func(x(i), y(i), params)
     k(2) = func(x(i) + 0.5_wp*h, y(i) + 0.5_wp*h*k(1), params)
     k(3) = func(x(i) + 0.5_wp*h, y(i) + 0.5_wp*h*k(2), params)
     k(4) = func(x(i) + h, y(i) + h*k(3), params)
     y(i+1) = y(i) + h * (k(1) + 2.0_wp * (k(2) + k(3)) + k(4)) / 6.0_wp
     y(i) = y(i+1)
  end do
  yf = y(n)
end subroutine rk4
