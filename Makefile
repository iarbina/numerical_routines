LIBNAME=numerical_routines

FC=gfortran
FFLAGS=-g -cpp -fPIC

ifeq ($(FC), gfortran)
  FFLAGS+=-fbacktrace -Wall -fimplicit-none -pthread
endif
ifeq ($(FC), ifort)
  FFLAGS+=-traceback -warn all -xHost -threads
endif

SRCS =$(notdir $(wildcard *.f*))
OBJS =$(addsuffix .o, $(basename $(SRCS)))

lib_stat: $(OBJS)
	ar crv lib$(LIBNAME).a $(OBJS)

lib_dyna: $(OBJS)
	$(FC) $(FFLAGS) -shared -o $(LIBNAME)lib.so $^

%.o: %.f90
	$(FC) $(FFLAGS) -c $<

clean:
	rm -f $(OBJS) lib$(LIBNAME).a $(LIBNAME)lib.so
