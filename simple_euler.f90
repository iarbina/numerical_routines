subroutine simple_euler (x0, y0, n, xf, func, params, yf)
  use, intrinsic :: iso_fortran_env, wp => real64
  integer,  intent(in)  :: n
  real(wp), intent(in)  :: x0, y0, xf, params(:)
  real(wp), intent(out) :: yf
  real(wp) :: h
  real(wp), allocatable :: x(:), y(:)
  integer :: i
  
  interface
    function func (x, y, params)
      import :: wp
      implicit none
      real(wp) :: func, x, y, params(:)
    end function func
  end interface
  
  if (allocated(x)) deallocate(x)
  if (allocated(y)) deallocate(y)
  allocate(x(n), y(n))
  
  h = (xf - x0) / (n - 1)
  x = [ (x0 + i * h, i=1, n) ]
  
  y(1) = y0
  do i=1, n-1
     y(i+1) = y(i) + h * func(x(i), y(i), params)
     y(i) = y(i+1)
  end do
  yf = y(n)
end subroutine simple_euler
